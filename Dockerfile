# Official Python base image is needed or some applications will segfault.
ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-alpine

# PyInstaller needs zlib-dev, gcc, libc-dev, and musl-dev
RUN apk --update --no-cache add \
    zlib-dev \
    musl-dev \
    libc-dev \
    gcc \
    git \
    pwgen \
    && pip install --upgrade pip

# Install pycrypto so --key can be used with PyInstaller
RUN pip install \
    pycrypto

ARG PYINSTALLER_BRANCH

# Build bootloader for alpine
RUN git clone --depth 1 --single-branch --branch ${PYINSTALLER_BRANCH} https://github.com/pyinstaller/pyinstaller.git /tmp/pyinstaller \
    && cd /tmp/pyinstaller/bootloader \
    && python ./waf configure --no-lsb all \
    && pip install --no-use-pep517 .. \
    && rm -Rf /tmp/pyinstaller \
    && mv /usr/local/bin/pyinstaller /usr/local/bin/_pyinstaller

COPY ./files /
